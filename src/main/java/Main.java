import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by amen on 8/18/17.
 */
public class Main {

    public static void main(String[] args){
        File naszPlik = new File("test.txt");
        try {
            Scanner sc = new Scanner(naszPlik);
            String liniaZPliku ;
            while (sc.hasNextLine()){
                liniaZPliku = sc.nextLine();
                System.out.println(liniaZPliku);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}